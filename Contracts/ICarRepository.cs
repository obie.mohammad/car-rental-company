﻿using Entities.Models;

namespace Contracts
{
    public interface ICarRepository : IRepositoryBase<Car>
    {
        /// <summary>
        /// Get a list of paginated, searched and sorted cars.
        /// </summary>
        Task<PagedList<Car>> GetAllAsync(int pageSize, int pageNumber, string keyWord, string orderBy);
        Task<Car> GetAsync(Guid id);

        Task<Car> CreateAsync(Car car);
    }
}
