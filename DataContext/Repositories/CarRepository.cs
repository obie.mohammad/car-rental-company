﻿using System.Linq.Dynamic.Core;
using System.Reflection;
using System.Text;
using Contracts;
using Entities.Models;
using Entities.Models.Exceptions;
using Microsoft.EntityFrameworkCore;

namespace DataContext.Repositories
{
    public class CarRepository : RepositoryBase<Car>, ICarRepository
    {
        public CarRepository(DataContext dataContext) : base(dataContext)
        {
        }

        public async Task<Car> CreateAsync(Car car)
        {
            Create(car);

            await SaveChangesAsync();

            return car;
        }

        public async Task<PagedList<Car>> GetAllAsync(int pageSize, int pageNumber, string keyWord, string orderBy)
        {
            var cars = FindAll();

            SearchByKeyWord(ref cars, keyWord);

            ApplySort(ref cars, orderBy);

            return await PagedList<Car>.ToPagedListAsync(cars, pageNumber, pageSize);
        }

        public async Task<Car> GetAsync(Guid id)
        {
            var car = await FindByCondition(x => x.Id == id).FirstOrDefaultAsync();

            if (car is null)
                throw new EntityNotFoundException(id.ToString());

            return car;
        }

        private void SearchByKeyWord(ref IQueryable<Car> Cars, string keyWord)
        {
            if (!Cars.Any() || string.IsNullOrWhiteSpace(keyWord))
                return;
            Cars = Cars.Where(o => o.Color.ToLower().Contains(keyWord.ToLower()) ||
            o.Type.ToLower().Contains(keyWord.ToLower()));
        }

        private void ApplySort(ref IQueryable<Car> cars, string orderBy)
        {
            if (!cars.Any())
                return;
            if (string.IsNullOrWhiteSpace(orderBy))
            {
                cars = cars.OrderBy(x => x.EngineCapacity);
                return;
            }
            var orderParams = orderBy.Trim().Split(',');
            var propertyInfos = typeof(Car).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            var orderQueryBuilder = new StringBuilder();
            foreach (var param in orderParams)
            {
                if (string.IsNullOrWhiteSpace(param))
                    continue;
                var propertyFromQueryName = param.Split(" ")[0];
                var objectProperty = propertyInfos.FirstOrDefault(pi => pi.Name.Equals(propertyFromQueryName, StringComparison.InvariantCultureIgnoreCase));
                if (objectProperty == null)
                    continue;
                var sortingOrder = param.EndsWith(" desc") ? "descending" : "ascending";
                orderQueryBuilder.Append($"{objectProperty.Name.ToString()} {sortingOrder}, ");
            }
            var orderQuery = orderQueryBuilder.ToString().TrimEnd(',', ' ');
            if (string.IsNullOrWhiteSpace(orderQuery))
            {
                cars = cars.OrderBy(x => x.EngineCapacity);
                return;
            }
            cars = cars.OrderBy(orderQuery);
        }
    }
}
