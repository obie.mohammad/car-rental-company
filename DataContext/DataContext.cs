﻿using Entities.Models;
using Microsoft.EntityFrameworkCore;

namespace DataContext
{
    public class DataContext : DbContext
    {
        internal DbSet<Car> Cars { get; set; }
        internal DbSet<Driver> Drivers { get; set; }
        internal DbSet<Customer> Customers { get; set; }
        internal DbSet<RentalCar> RentalCars { get; set; }

        public DataContext(DbContextOptions options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<RentalCar>().HasKey(rc => new { rc.CarId, rc.CustomerId });

            modelBuilder.Entity<RentalCar>()
             .HasOne(rc => rc.Customer)
             .WithMany(c => c.Cars)
             .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<RentalCar>()
             .HasOne(rc => rc.Car)
             .WithMany()
             .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
