﻿using AutoMapper;
using Entities.DataTransferObjects;
using Entities.Models;

namespace CarRentalCo
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<CreateCarCommandDto, Car>();
            CreateMap<UpdateCarCommandDto, Car>();
            CreateMap<Car, CarDto>();
        }
    }
}
