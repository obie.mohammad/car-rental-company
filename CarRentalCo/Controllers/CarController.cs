﻿using AutoMapper;
using Contracts;
using Entities.DataTransferObjects;
using Entities.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;

namespace CarRentalCo.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CarController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly ICarRepository _carRepository;
        private IMemoryCache _cache;
        private const string TopEngineCapacity = "topEngineCapacity";

        public CarController(IMapper mapper,
             IMemoryCache cache,
            ICarRepository carRepository)
        {
            _carRepository = carRepository;
            _cache = cache;
            _mapper = mapper;
        }

        [HttpPost]
        [Route("Create")]
        public async Task<ActionResult<CarDto>> CreateAsync([FromForm] CreateCarCommandDto input)
        {
            var car = _mapper.Map<Car>(input);

            if (await _carRepository.FindByCondition(x => x.Number == input.Number).FirstOrDefaultAsync() != null)
                throw new Exception($"Name {input.Number} is already exists.");

            car.Id = Guid.NewGuid();

            await _carRepository.CreateAsync(car);

            _cache.Remove(TopEngineCapacity);

            return Ok(_mapper.Map<CarDto>(car));
        }

        [HttpPut]
        [Route("Update")]
        public async Task<ActionResult<CarDto>> UpdateAsync([FromForm] UpdateCarCommandDto input)
        {
            var car = await _carRepository.GetAsync(input.Id);

            if (await _carRepository.FindByCondition(x => x.Number == input.Number && input.Id != x.Id).FirstOrDefaultAsync() != null)
                throw new Exception($"Number {input.Number} is already exists.");

            _mapper.Map(input, car);

            _carRepository.Update(car);

            await _carRepository.SaveChangesAsync();

            _cache.Remove(TopEngineCapacity);

            return Ok(_mapper.Map<CarDto>(car));
        }

        [HttpGet]
        [Route("Get")]
        public async Task<ActionResult<CarDto>> GetAsync(Guid id)
        {
            var car = await _carRepository.GetAsync(id);

            return Ok(_mapper.Map<CarDto>(car));
        }

        /// <summary>
        /// Get all cars paginated, searched and sorted.
        /// </summary>
        [HttpGet]
        [Route("GetAll")]
        public async Task<ActionResult<ListPageResponseDto<CarDto>>> GetAllAsync([FromQuery] ListPageRequestDto request)
        {
            var pagedList = await _carRepository.GetAllAsync(request.PageSize, request.PageNumber, request.Keyword, request.OrderBy);

            return Ok(new ListPageResponseDto<CarDto>(_mapper.Map<List<CarDto>>(pagedList), pagedList.TotalCount));
        }

        /// <summary>
        /// Get cars with the top engine capacity, from the cache memory if exist.
        /// </summary>
        [HttpGet]
        [Route("GetTopEngineCapacityCars")]
        public async Task<ActionResult<List<CarDto>>> GetAllCachedAsync()
        {
            if (!_cache.TryGetValue(TopEngineCapacity, out PagedList<Car> pagedList))
            {
                pagedList = await _carRepository.GetAllAsync(10, 1, string.Empty, "engineCapacity desc");

                var cacheEntryOptions = new MemoryCacheEntryOptions()
                        .SetSlidingExpiration(TimeSpan.FromSeconds(60))
                        .SetAbsoluteExpiration(TimeSpan.FromSeconds(3600))
                        .SetPriority(CacheItemPriority.Normal)
                        .SetSize(1024);
                _cache.Set(TopEngineCapacity, pagedList, cacheEntryOptions);
            }

            return Ok(_mapper.Map<List<CarDto>>(pagedList));
        }

        [HttpDelete]
        [Route("Delete")]
        public async Task<ActionResult<bool>> DeleteAsync(Guid id)
        {
            var car = await _carRepository.GetAsync(id);

            // Check if there is any active rental cars.

            _carRepository.Delete(car);

            await _carRepository.SaveChangesAsync();

            return Ok(true);
        }
    }
}
