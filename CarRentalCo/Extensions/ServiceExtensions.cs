﻿using Contracts;
using DataContext.Repositories;
using LoggerService;
using Microsoft.EntityFrameworkCore;

namespace CarRentalCo.Extensions
{
    public static class ServiceExtensions
    {
        public static void ConfigureCors(this IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader());
            });
        }

        public static void ConfigureLoggerService(this IServiceCollection services)
        {
            services.AddSingleton<ILoggerManager, LoggerManager>();
        }

        public static void ConfigureSqlContext(this IServiceCollection services, IConfiguration config)
        {
            services.AddDbContext<DataContext.DataContext>(o => o.UseSqlServer(
                config.GetConnectionString("DefaultConnection")));
        }

        public static void ConfigureDependencies(this IServiceCollection services)
        {
            services.AddScoped<ICarRepository, CarRepository>();
        }
    }
}
