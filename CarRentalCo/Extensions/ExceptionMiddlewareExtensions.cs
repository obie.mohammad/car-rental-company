﻿using System.ComponentModel.DataAnnotations;
using System.Net;
using Contracts;
using Entities.Models.Exceptions;
using Microsoft.AspNetCore.Diagnostics;

namespace CarRentalCo.Extensions
{
    public static class ExceptionMiddlewareExtensions
    {
        public static void ConfigureExceptionHandler(this IApplicationBuilder app, ILoggerManager logger)
        {
            app.UseExceptionHandler(appError =>
            {
                appError.Run(async context =>
                {
                    context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                    context.Response.ContentType = "application/json";
                    var contextFeature = context.Features.Get<IExceptionHandlerFeature>();
                    if (contextFeature != null)
                    {
                        logger.LogError($"Something went wrong: {contextFeature.Error}");

                        ErrorDetails error;
                        var exception = contextFeature.Error;

                        switch (exception)
                        {
                            case ValidationException validationException:
                                error = new ErrorDetails((int)HttpStatusCode.BadRequest, validationException.Message + ", " + validationException.ValidationResult.MemberNames);
                                context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                                break;
                            case EntityNotFoundException notFoundException:
                                error = new ErrorDetails((int)HttpStatusCode.NotFound, notFoundException.Message);
                                context.Response.StatusCode = (int)HttpStatusCode.NotFound;
                                break;
                            default:
                                error = new ErrorDetails((int)HttpStatusCode.InternalServerError, "Internal Server Error");
                                context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                                break;
                        }

                        await context.Response.WriteAsync(error.ToString());
                    }
                });
            });
        }
    }
}
