﻿namespace Entities.DataTransferObjects
{
    public class CarDto
    {
        public Guid Id { get; set; }

        public string Number { get; set; }

        public string Type { get; set; }

        public int EngineCapacity { get; set; }

        public string Color { get; set; }

        public double DailyFare { get; set; }

        public bool WithDriver { get; set; } = true;

        public Guid? DriverId { get; set; }
    }
}
