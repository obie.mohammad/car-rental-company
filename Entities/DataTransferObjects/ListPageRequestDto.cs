﻿namespace Entities.DataTransferObjects
{
    public class ListPageRequestDto
    {
        public int PageNumber { get; set; } = 1;


        const int MaxPageSize = 50;

        public string Keyword { get; set; }

        public string OrderBy { get; set; }

        private int _pageSize = 10;

        public int PageSize
        {
            get
            {
                return _pageSize;
            }
            set
            {
                _pageSize = (value > MaxPageSize) ? MaxPageSize : value;
            }
        }
    }
}
