﻿using System.ComponentModel.DataAnnotations;

namespace Entities.DataTransferObjects
{
    public class CreateCarCommandDto : IValidatableObject
    {
        [Required]
        [RegularExpression("([0-9]+)")]
        public string Number { get; set; }

        [Required]
        [StringLength(100)]
        public string Type { get; set; }

        [Range(1, 4000, ErrorMessage = "Only positive number allowed")]
        public int EngineCapacity { get; set; }

        [StringLength(100)]
        public string Color { get; set; }

        public double DailyFare { get; set; }

        public bool WithDriver { get; set; }

        public Guid? DriverId { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var results = new List<ValidationResult>();

            return results;
        }
    }
}
