﻿namespace Entities.DataTransferObjects
{
    public class ListPageResponseDto<T>
    {
        public int TotalCount { get; private set; }

        public List<T> Items { get; set; }

        public ListPageResponseDto(List<T> items, int count)
        {
            TotalCount = count;
            Items = items;
        }
    }
}
