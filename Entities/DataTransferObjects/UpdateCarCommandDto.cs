﻿using System.ComponentModel.DataAnnotations;

namespace Entities.DataTransferObjects
{
    public class UpdateCarCommandDto
    {
        public Guid Id { get; set; }

        [Required]
        [RegularExpression("([0-9]+)")]
        public string Number { get; set; }

        [Required]
        [StringLength(100)]
        public string Type { get; set; }

        [Range(1, 4000, ErrorMessage = "Only positive number allowed")]
        public int EngineCapcity { get; set; }

        [StringLength(100)]
        public string Color { get; set; }

        public double DailyFare { get; set; }

        public bool WithDriver { get; set; }

        public Guid? DriverId { get; set; }
    }
}
