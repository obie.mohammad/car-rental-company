﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities.Models
{
    public class Car
    {
        public Guid Id { get; set; }

        [StringLength(100)]
        public string Number { get; set; }

        [StringLength(100)]
        public string Type { get; set; }

        public int EngineCapacity { get; set; }

        [StringLength(100)]
        public string Color { get; set; }

        public double DailyFare { get; set; }

        public bool WithDriver { get; set; } = true;

        public Guid? DriverId { get; set; }

        [ForeignKey(nameof(DriverId))]
        public virtual Driver Driver { get; set; }
    }
}
