﻿using Microsoft.AspNetCore.Identity;

namespace Entities.Models
{
    public class Customer : IdentityUser<Guid>
    {
        public virtual ICollection<RentalCar> Cars { get; set; }
    }
}
