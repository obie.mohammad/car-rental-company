﻿using Microsoft.EntityFrameworkCore;

namespace Entities.Models
{
    public class PagedList<T> : List<T>
    {
        public int TotalCount { get; private set; }

        public PagedList(List<T> items, int count)
        {
            TotalCount = count;

            AddRange(items);
        }

        public async static Task<PagedList<T>> ToPagedListAsync(IQueryable<T> source, int pageNumber, int pageSize)
        {
            var count = await source.CountAsync();
            var items = await source.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync();
            return new PagedList<T>(items, count);
        }
    }
}
