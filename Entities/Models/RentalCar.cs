﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Entities.Models
{
    public class RentalCar
    {
        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public decimal Cost { get; set; }

        public bool IsActive { get; set; } = true;

        public Guid CustomerId { get; set; }

        public Guid CarId { get; set; }

        public Guid? DriverId { get; set; }

        [ForeignKey(nameof(CarId))]
        public virtual Car Car { get; set; }

        [ForeignKey(nameof(CustomerId))]
        public virtual Customer Customer { get; set; }

        [ForeignKey(nameof(DriverId))]
        public virtual Driver Driver { get; set; }
    }
}
