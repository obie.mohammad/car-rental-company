﻿using Microsoft.AspNetCore.Identity;

namespace Entities.Models
{
    public class Driver : IdentityUser<Guid>
    {
        public virtual Car Car { get; set; }
    }
}
