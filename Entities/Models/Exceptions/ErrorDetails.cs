﻿using System.Text.Json;

namespace Entities.Models.Exceptions
{
    public class ErrorDetails
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }

        public ErrorDetails(int statusCode, string message)
        {
            Message = message;
            StatusCode = statusCode;
        }

        public override string ToString()
        {
            return JsonSerializer.Serialize(this);
        }
    }

}
