﻿namespace Entities.Models.Exceptions
{
    public class EntityNotFoundException : Exception
    {
        public EntityNotFoundException(string id) : base($"Entity with Id = '{id}' was not found.")
        {
        }
    }
}
